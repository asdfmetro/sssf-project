'use strict'

let originalData = null;

// unused basic google MAP setup
function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 2,
    center: {lat: 0, lng: -180},
    mapTypeId: 'terrain'
  });
  var contentString = 'You were here at 12:00';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
  var marker = new google.maps.Marker({
    position: {lat: 0, lng: -180},
    map: map,
    title: 'Uluru (Ayers Rock)'
  });
  var marker2 = new google.maps.Marker({
    position: {lat: 37, lng: -122},
    map: map,
    title: 'OULALALALALALALALALA'
  });
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });


  var flightPlanCoordinates = [
    {lat: 37.772, lng: -122.214},
    {lat: 21.291, lng: -157.821},
    {lat: -18.142, lng: 178.431},
    {lat: -27.467, lng: 153.027}
  ];
  var flightPath = new google.maps.Polyline({
    path: flightPlanCoordinates,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2
  });

  flightPath.setMap(map);
}


// function to get the data for the api and launch the updateData()
const getData = () => {
  fetch('/api/me/logs/recent/10',{
    credentials: "same-origin"
  }).then(response => {
    return response.json();
  }).then(items => {
    //console.log(items);
    if(items[0]['status'] != 'OK')
    {
      alert('problem with the data getting !');
    }
    else {
      originalData = items;
      updateData(items);
    }
  });
};
// function to update the data on the front end (charts, map)
const updateData = (datas) => {
  console.log(datas);
  // init the different variable for the data
  let dataSpeed = [];
  let datanbStep = [];
  let dataTime = [];
  let dataGPS = []
  datas.forEach((item)=>{
    // don't take the status code
    if(item['status'] !== 'OK')
    {
      //console.log(item['nbStep']);
      datanbStep.push(item['nbStep']);
      dataTime.push(item['date']);
      dataSpeed.push(item['speed']);
      dataGPS.push( item['GPS'] );
    }
  });
  //console.log(dataGPS);
  // set the data for the nbstep chart
  myStepChart.data.labels = dataTime;
  myStepChart.data.datasets[0].data = datanbStep;
  myStepChart.update();
  // set the data for the Speed chart
  mySpeedChart.data.labels = dataTime;
  mySpeedChart.data.datasets[0].data = dataSpeed;
  mySpeedChart.update();

  // set the map following path and marker with date
  // take the middle GPS coordinate to center the map on it
  console.log("number GPS point : "+dataGPS.length);
  const centerPoint = parseInt(dataGPS.length/2); // if the number is odd parse int
  console.log(dataGPS[centerPoint].coord);
  const map = new google.maps.Map(document.getElementById('map'), {
    zoom: 11,
    center: dataGPS[centerPoint], //{lat: 0, lng: -180}
    mapTypeId: 'terrain'
  });
  // set all the marker
  let i = 0;
  dataGPS.forEach((item)=>{
    console.log(item);
    // set up text for the marker
    let contentStringtmp = '<h3>'+dataTime[i]+'</h3>'+
    '<p>Your current speed was : '+dataSpeed[i]+' km/h <br />And your number of steps was : '+datanbStep[i]+'</p>';
    // set up text box for the marker
    let infowindowtmp = new google.maps.InfoWindow({
      content: contentStringtmp
    });
    // set up marker
    let markertmp =  new google.maps.Marker({
      position: item,
      map: map,
      title: 'Point at'+ dataTime[i]
    });
    // when we click on the marker print the text
    markertmp.addListener('click', () =>{
      infowindowtmp.open(map, markertmp);
    });
    i++;
  });

  //set the following path
  const followingPath = new google.maps.Polyline({
    path: dataGPS,
    geodesic: true,
    strokeColor: '#0000FF',
    strokeOpacity: 1.0,
    strokeWeight: 2
  });
  followingPath.setMap(map);
  //console.log(dataSpeed);
}

// init the charts
let ctx = document.getElementById("stepschart").getContext("2d");
let ctx2 = document.getElementById("speedchart").getContext("2d");
let config1 = {
  type: 'line',
  data: {
    labels: [],
    datasets: [{
      label: 'Step number',
      data: [],
      backgroundColor: "rgba(153,255,51,0.4)"
    }]
  },
  options:
  {
    scales:
    {
      xAxes: [{
        display: false
      }]
    }
  }
};
let config2 = {
  type: 'line',
  data: {
    labels: [],
    datasets: [{
      label: 'Speed',
      data: [],
      backgroundColor: "rgba(255,153,0,0.4)"
    }]
  },
  options:
  {
    scales:
    {
      xAxes: [{
        display: false
      }]
    }
  }
}

let myStepChart = new Chart(ctx, config1);
let mySpeedChart = new Chart(ctx2, config2);




//initMap();
getData();
