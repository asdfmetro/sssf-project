'use strict';

const submitLogin = () =>{
  document.getElementById('loginerroremail').style.display = 'none';
  const url = '/login/me';
  const formData = JSON.stringify({
    username: document.getElementById('loginInputEmail').value,
    password: document.getElementById('loginInputPassword').value
  });
  console.log(formData);
  fetch(url, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    },
    body: formData,
    credentials: 'same-origin'
  }).then((resp) => {
    return resp.json();
  }).then((json) => {
    console.log(json);
    if(json.status === "ERROR"){
      document.getElementById('loginerroremail').style.display = 'block';
    }
    else {
      window.location = '/';
    }
  });
  return false;
}

const submitSignin = () => {
  //document.getElementById('loginerroremail').style.display = 'none';
  const url = '/login/signin';
  let send=true;
  // check value of the different input

  let username = document.getElementById('signinInputUsername').value;
  let email = document.getElementById('signinInputEmail').value;
  let reemail = document.getElementById('signinInputRetypeEmail').value;
  let password = document.getElementById('signinInputPassword').value;
  let repassword = document.getElementById('signinInputRetypePassword').value;

  // check if username is not taken

  checkUserFree(username, (status)=>{
    if(!status)
    {
      send=false;
    }
  });

  // check the 2 mail input (valid and same)
  // to add --> email regex verif
  if(email !== reemail || email === '' )
  {

    document.getElementById('signinErrorRetypeEmail').style.display = 'block';
    document.getElementById('signinErrorRetypeEmail').className = 'alert alert-danger';
    document.getElementById('signinErrorRetypeEmail').innerHTML =' The 2 email are not the same !';
    send=false;
  }
  else {
    document.getElementById('signinErrorRetypeEmail').style.display = 'block';
    document.getElementById('signinErrorRetypeEmail').className = 'alert alert-success';
    document.getElementById('signinErrorRetypeEmail').innerHTML =' The 2 email are the same !';
  }

  // check the 2 input password / difficulities
  if(password !== repassword || password === '' )
  {

    document.getElementById('signinErrorRetypePassword').style.display = 'block';
    document.getElementById('signinErrorRetypePassword').className = 'alert alert-danger';
    document.getElementById('signinErrorRetypePassword').innerHTML =' The 2 password are not the same !';
    send=false;
  }
  else {
    document.getElementById('signinErrorRetypePassword').style.display = 'block';
    document.getElementById('signinErrorRetypePassword').className = 'alert alert-success';
    document.getElementById('signinErrorRetypePassword').innerHTML =' The 2 password are the same !';
  }




  if(!send){
    return false;
  }
  alert('go send');
  const formData = JSON.stringify({
    username: username,
    email: email,
    password: password
  });
  console.log(formData);
  fetch(url, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    },
    body: formData,
    credentials: 'same-origin'
  }).then((resp) => {
    return resp.json();
  }).then((json) => {
    console.log(json);
    if(json.status === "ERROR"){
      alert('error in the creation of the account, please try later');
    }
    else {
      window.location = '/login';
    }
  });
  return false;
}
const checkUserFree = (username,callback) => {
  const formData = JSON.stringify({
    username: username
  });
  fetch('/login/user', {
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    },
    body: formData,
    credentials: 'same-origin'
  }).then((resp) => {
    return resp.json();
  }).then((json) => {
    console.log(json);
    if(json.status === "free"){
      document.getElementById('signinErrorUsername').style.display = 'block';
      document.getElementById('signinErrorUsername').className = 'alert alert-success';
      document.getElementById('signinErrorUsername').innerHTML =' This username is free';
      return callback(true);
    }
    else {
      document.getElementById('signinErrorUsername').style.display = 'block';
      document.getElementById('signinErrorUsername').className = 'alert alert-danger';
      document.getElementById('signinErrorUsername').innerHTML =' This username is not  free';
      return callback(false);
    }
  });

}
