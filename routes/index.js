var express = require('express');
const path = require('path');
var router = express.Router();

/* GET home page. */
router.get('/', (req, res) =>{
  //res.render('index', { title: 'Express' });
  res.render('index', { username: req.session.currentUser });
});
router.get('/mydata', (req, res) =>{
  //res.render('index', { title: 'Express' });
  res.sendFile(path.resolve('public/overview.html'));
});

module.exports = router;
