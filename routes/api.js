const express = require('express');
const router = express.Router();
const DB = require('../database');
const crypto = require('crypto');


/**
 * @api {get} /api/me/get/token request to get the token from a user authentification
 * @apiName GetToken
 * @apiGroup Token
 *
 * @apiParam {String} username   username
 * @apiParam {String} password       password

 * @apiParamExample {json} Request-Example:
 *    {
 *       "username": "test123",
 *       "password": "test"
 *     }
 *
 * @apiSuccess {String} status state of the request.
 *
 * @apiSuccessExample Success-Response:
 *
 *     {
 *       "status" : "OK",
 *       "token" : "36d8b2403952240b5a6ddd6f96a2fd7b6d9be8148cccf24d95a3ec3e6ca62dfd"
 *     }
 *
 * @apiError Problem to save the data.
 *
 * @apiErrorExample Error-Response:
 *     {
 *       "status":"wrong username or password"
 *     }
 *  @apiExample {curl} Curl command example, to get the token :
 *    curl -X GET -H  "Content-Type: application/json" -d '{ "username": "test123", "password":"test"}' https://localhost/api/me/get/token --insecure
 */
router.get('/me/get/token', (req, res) =>{
	const username = req.body.username;
	const password = req.body.password;
	DB.chekUserLogin(username, password, (err,user) =>{
		if(err){
			res.send({"status" : "wrong username or password"});
		}
		else {
			const secret  = user.username+':string to add sdlkdsfjsd:'+Date.now();
			const token = crypto.createHmac('sha256', secret).digest('hex');
			user.last_token.value = token;
			user.last_token.date = Date.now();
			user.save((err) =>{
				if (err)
				{
					console.log('error token change');
					res.send('{"status" : "error"}');
				}
				else
				{
					console.log('success token change');
					res.send('{"status" : "OK", "token" : "'+token+'"}');
				}
			});
			console.log(user);
		}
	});
});
// middleware to check if the token/session is good in the request
// and set the id of the user
router.use('/(*?/?)*', (req,res,next)=>{
  console.log(req.session.connected);
	// if token is not in the request and if the user is not connected --> ERROR
	if(req.body.token == null && !req.session.connected)
	{
		// erreur
		console.log('error with the token!');
		res.status(300).send({ 'status' : 'inexistante or wrong token'});
	}
	// if user connected get the user id
	else if (req.session.connected) {
		// find the user and set the userref
		DB.getIdUser(req.session.currentUser,(err, user)=>{
			if (err){
				console.log('erreur ');
				res.status(300).send('{"status" : "error incorrect username"}');
				return ;
			}
			req.body.userref = user._id;
			next();
		});

	}
	// check the token in the database
	else {
		DB.checktokenuser(req.body.token, (err, user)=>{
			if (err){
				console.log('token not valid');
				res.status(300).send({"status" : "error token"});
				return ;
			}
			req.body.userref= user._id;
			console.log(req.body.token);
			next();
		})
	}
});


/**
 * @api {put} /me/logs/add Create a new log entrie
 * @apiName NewLog
 * @apiGroup Log
 *
 * @apiParam {String} token   user token (doesn't implement rigth now).
 * @apiParam {json} GPS       GPS data :  {"lat": 50,"lng": 50}
 * @apiParam {Number} nbStep  number of step.
 * @apiParam {Number} speed   current speed of the user.
 * @apiParamExample {json} Request-Example:
 *    {
 *       "token": "kfjsdlkfjdsfljsdqlmfkjdflmkj",
 *       "GPS": {"lat": 50,"lng": 50},
 *       "nbStep": 50,
 *       "speed": 5
 *     }
 *
 * @apiSuccess {String} status state of the request.
 *
 * @apiSuccessExample Success-Response:
 *
 *     {
 *       "status": "OK"
 *     }
 *
 * @apiError Problem to save the data.
 *
 * @apiErrorExample Error-Response:
 *     {
 *       "status": "ERROR"
 *     }
 *  @apiExample {curl} Curl command example, to add a new data :
 *    curl -X PUT -H  "Content-Type: application/json" -d '{ "token": "kfjsdlkfjdsfljsdqlmfkjdflmkj","GPS": {"lat": "10","lng": "10"},"nbStep": "10","speed": 5}' https://localhost/me/logs/add --insecure
 */
router.put('/me/logs/add', (req, res) =>{
	DB.createdatauser(req.body, (err)=>{
		if (err){
			console.log('erreur ');
			res.status(300).send('{"status" : "error"}');
			return ;
		}
		res.send({"status" : "OK"});
	});
});

/**
 * @api {get} /me/logs/recent/[:nblastdata] get the last logs
 * @apiName SeeLog
 * @apiGroup Log
 *
 * @apiParam {String} token   user token (doesn't implement rigth now).
 * @apiParam {Number} :nblastdata   number of the last data that you want if this number is not present, the default value is 10.
 * @apiParamExample {json} Request-Example:
 *    {
 *       "token": "kfjsdlkfjdsfljsdqlmfkjdflmkj"
 *     }
 *
 * @apiSuccess {String} return the last logs.
 *
 * @apiSuccessExample Success-Response:
 *
 *     [{"_id":"58e758ba59a84a11330a300c","userRef":0,"nbStep":10,"speed":5,"__v":0,"GPS":{"lat":10,"lng":10},"date":"2017-04-07T09:15:36.593Z"}]
 *
 * @apiError Problem to save the data.
 *
 * @apiErrorExample Error-Response:
 *     {
 *       "status": "ERROR"
 *     }
 *  @apiExample {curl} Curl command example to see the last 2 data :
 *    curl -X GET -H  "Content-Type: application/json" -d '{ "token": "kfjsdlkfjdsfljsdqlmfkjdflmkj" }' http://localhost:3000/me/logs/recent/2  --insecure
 */
router.get('/me/logs/recent/:nblastdata?', (req, res) =>{
  if(req.params.nblastdata == null)
  {
      req.params.nblastdata=10;
  }
	// return the last nblastdata from the data base for the user
	DB.getDataUser(req.body.userref,req.params.nblastdata, (err, datas)=>{
		if (err){
			console.log('erreur ');
			res.status(300).send('{"status" : "error"}');
			return ;
		}
		res.send(JSON.stringify(datas));
	})


});


router.get('/', function(req, res, next) {
	res.send({"status" : "OK"});
});
router.get('/*', function(req, res, next) {
	res.send({"status" : "Wrong Request"});
});

module.exports = router;
