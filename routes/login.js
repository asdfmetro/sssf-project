const express = require('express');
const path = require('path');
const DB = require('../database');
const router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) =>{
  //res.render('index', { title: 'ldkjsfsmlkfhqsdùoifhsqdùofihdùooij' });
  if(req.session.connected)
  {
    res.redirect('/');
  }
  res.sendFile(path.resolve('public/login.html'));
});
router.get('/getout', (req, res, next) =>{
  //res.render('index', { title: 'ldkjsfsmlkfhqsdùoifhsqdùofihdùooij' });
  if(req.session.connected)
  {
    req.session.connected = false;
    req.session.currentUser=null;
    req.session.save();
  }
  res.redirect('/login');
});
/*router.post('/me', function(req, res, next) {
  res.send(JSON.stringify(req));
});*/
router.post('/me',(req, res, next) =>{

  //res.render('index', { title: 'ldkjsfsmlkfhqsdùoifhsqdùofihdùooij' });
  const username = req.body.username;
	const password = req.body.password;
  console.log(req.body);
	DB.chekUserLogin(username, password, (err,user) =>{
		if(err){
			res.send({'status': 'ERROR'});
		}
		else {
      req.session.connected=true;
      req.session.currentUser=user.username;
      req.session.save();
			console.log(user);
      res.send({'status': 'OK'});
		}
	});
});
router.post('/user',(req, res, next) =>{

  //res.render('index', { title: 'ldkjsfsmlkfhqsdùoifhsqdùofihdùooij' });
  const username = req.body.username;
	const password = req.body.password;
  console.log(req.body);
	DB.chekUserFree(username, (err) =>{
		if(err){
			res.send({'status': 'free'});
		}
		else {
      res.send({'status': 'taken'});
		}
	});
});

router.post('/signin',(req, res, next) =>{

  //res.render('index', { title: 'ldkjsfsmlkfhqsdùoifhsqdùofihdùooij' });
  const username = req.body.username;
  const email = req.body.email;
	const password = req.body.password;
  console.log(req.body);
	DB.createuser(username, email,password, (err) =>{
		if(!err){
			res.send({'status': 'ERROR'});
		}
		else {
      res.send({'status': 'OK'});
		}
	});
});


module.exports = router;
