z=0
nbsteep=0
lat=0
lng=0
speed=5
token='36d8b2403952240b5a6ddd6f96a2fd7b6d9be8148cccf24d95a3ec3e6ca62dfd'
while [ 1 ]
do
	xstep=$(($nbsteep))
	xlat=$(($lat))
	xlng=$(($lng))
	curl -X PUT -H  "Content-Type: application/json" -d "{ \"token\": \"$token\",\"GPS\": {\"lat\": \"$(($xlat))\",\"lng\": \"$(($xlng))\"},\"nbStep\": \"$(($xstep))\",\"speed\": 5}" https://localhost/api/me/logs/add --insecure
	z=$(($z+1))
	lat=$((($lat+30)%3000))
	lng=$((($lng+10)%3000))
	nbsteep=$((($nbsteep+10)%1000))
	echo ''
	sleep 30
done
