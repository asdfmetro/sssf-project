'use strict';
const crypto = require('crypto');

class Database {
  constructor() {
    this.userSchema = {
      username: String,
      email: String,
      hashpass: String,
      last_token: { value: { type: String, default: null}, date: { type: Date, default: Date.now()} }
    };
    this.dataUserSchema = {
      userRef: String,
      date: { type: Date, default: Date.now()},
      GPS: { lat: Number, lng: Number},
      nbStep: Number,
      speed: Number
    };
    this.mongoose = require('mongoose');
    this.mongoose.Promise = global.Promise; //ES6 Promise
    this.user = this.getSchema(this.userSchema, 'Users');
    this.datauser = this.getSchema(this.dataUserSchema, 'Data');
  };

  connect(url, app, http, https, options) {
    this.url = url;
    this.app = app;
    this.mongoose.connect(this.url).then(() => {
      console.log('Connected to Mongo');
      //this.app.listen(3000);

      // http redirection to https
      http.createServer((req, res) => {
        res.writeHead(301, { 'Location': 'https://localhost' + req.url });
        res.end();
      }).listen(80);

      https.createServer(options, app).listen(443);
    }, (err) => {
      console.log(err.message);
      console.error('Connecting to Mongo failed');
    });
  };

  getSchema(schema, name) {
    const s = new this.mongoose.Schema(schema);
    return this.mongoose.model(name, s);
  };
  // create a user with the password hashed in sha256
  createuser(username,email, password, callback) {
    const tmpuser = new this.user({ username: username, email: email, hashpass: crypto.createHmac('sha256', password).digest('hex') });
    tmpuser.save( (err) => {
      if (err) {
        console.log(err);
        return callback(false);
      } else {
        console.log('meow');
        return callback(true);
      }
    });
  };
  // verify the login of the user
  chekUserLogin(username, password, callback)
  {
    this.user.findOne({ username: username }, (err, user) =>{
      if (err) {
        console.log('errreor');
        return callback(true,null);
      }
      if (!user) {
        console.log('user doesnt exist');
        return callback(true,null);
      }
      if (user.hashpass !== crypto.createHmac('sha256', password).digest('hex') ) {
        console.log('pass not good '+user.hashpass +' '+password)
        return callback(true,null);
      }
      console.log('good');
      return callback(false,user);
    });
  }
  // check if the username wanted is free or not (username uniq)
  chekUserFree(username, callback){
    this.user.findOne({ username: username }, (err, user) => {
      if (err) {
        console.log('errreor');
        return callback(true);
      }
      if (!user) {
        console.log('no user with this username');
        return callback(true);
      }

      console.log('user with this username');
      return callback(false);
    })
  }
  // check the token of the user is valid (equal and expired)
  checktokenuser(token, callback)
  {
    this.user.findOne({ 'last_token.value': token }, (err, user) => {
      if (err) {
        console.log('errreor');
        return callback(true,null);
      }
      if (!user) {
        console.log('no user with this token');
        return callback(true,null);
      }
      // check validity of the token
      // result in millisecond
      const differenceTime = Date.now() - user.last_token.date
      // if the different is higther than 2h = 120 minutes = 7200000 millisecond
      if(differenceTime > 7200000 )
      {
        console.log('token have to be regenerate');
        return callback(true,null);
      }

      console.log('good token');
      return callback(false,user);
    })
  }
  // get the id of the user
  getIdUser(username, callback)
  {
    this.user.findOne({ username: username}, (err,user) =>{
      if (err) {
        console.log('errreor');
        return callback(true,null);
      }
      if (!user) {
        console.log('no user with this token');
        return callback(true,null);
      }
      return callback(false,user);
    })
  }
  // get the number of data from a user
  getDataUser(iduser, number, callback)
  {
    this.datauser.find({
      	userRef: iduser
    	}).limit( Number(number)).sort({
    		date: -1
    	}).exec((err, datas)=>{
    		if(err){
    			return callback(true,null);
    		}
        // add the status at the begining
        datas.unshift( {'status': 'OK'});
        return callback(false,datas);
    	});
  }


  // add data to an user
  createdatauser(reqparam, callback ) {
    const userRef = reqparam.userref;
  	const gps = reqparam.GPS;
  	const nbStep = reqparam.nbStep;
  	const speed = reqparam.speed;
    // set the new data
  	const mynewdata = new this.datauser({
  		userRef: userRef,
  		GPS: gps,
  		nbStep: nbStep,
  		speed: speed,
      date: Date.now()
  	});
    console.log(mynewdata);
    // save it
  	mynewdata.save( (err) =>{
  	  if (err) {
        callback(err);
  	  } else {
        callback(null);
  	  }
  	});
  };
}

module.exports = new Database();
